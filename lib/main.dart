import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

import 'game.dart';
Socket socket;
List<String> g_c1=[],game_chain = ["", "", "", "", "", "", "", "", ""];

String player;

void main() {

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: splashscreen()
    );
  }
}
class splashscreen extends StatefulWidget {


  @override
  _splashscreenState createState() => _splashscreenState();
}

class _splashscreenState extends State<splashscreen> {
  @override
  void initState() {
    super.initState();
    // Timer(
    //     Duration(seconds: 2),
    //         () => Navigator.of(context).pushReplacement(MaterialPageRoute(
    //         builder: (BuildContext context) => Board())));

  }
  _connect()async{
    socket = await Socket.connect('176.231.64.112', 8010);
    // socket.add(utf8.encode('hello'));
    return socket;

    // socket.close();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(100.0),
        child: Container(
          child: ElevatedButton(onPressed: ()async{
            var socket= await _connect();
            game_chain=["", "", "", "", "", "", "", "", ""];

            Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => Board(socket)));},
              child: Text("start")),
        ),
      ),
    );
  }
}


class Board extends StatefulWidget {
  var socket;
  Board(this.socket);


  @override
  _BoardState createState() => _BoardState(socket);
}

class _BoardState extends State<Board> {
  int counter = 0;
  bool myturn=false;
  List<String> myplay=["", "", "", "", "", "", "", "", ""];
Socket socket;
  _BoardState(this.socket);



@override
  void initState() {
    // TODO: implement initState
  
  socket.listen((data)  {
    var x=String.fromCharCodes(data).trim();
    print(x);
    if (x.contains("!"+player.toString())) {
      myturn = true; print("x==player");
    }
    if(x.contains("player x")) {player = "x"; myturn=true;print('x.contains("player x")');}
    else if(x.contains("player o")) {
        player = "o"; print('x.contains("player o")');
      }  else if (x.length>10){
      game_chain=x.substring(1,x.length-1).replaceAll(new RegExp(r'\s'), "").split(",");
      print("11  "+game_chain.toString());
      myplay=game_chain;

      check_for_win();}
    setState(() {});
  });
    super.initState();
  }
  @override
  void dispose() {
    super.dispose();
  }
tapped(int index)  {
    if(game_chain[index]!="x"&&game_chain[index]!="o")
    {myplay[index]=player;
    print("changed");
    socket.add(utf8.encode(myplay.toString()));
    myturn=false;
    counter++;
    // check_for_win();

    }

}
check_for_win(){
     for (var i = 0; i < 9; i+=3){
      if(game_chain[i]==game_chain[i+1]&&game_chain[i]==game_chain[i+2]&&game_chain[i]!="")
      win(game_chain[i]);}
     for (var i = 0; i < 3; i++){
      if(game_chain[i]==game_chain[i+3]&&game_chain[i]==game_chain[i+6]&&game_chain[i]!="")
        win(game_chain[i]);
    }

     if (game_chain[0] == game_chain[4] &&
         game_chain[0] == game_chain[8] &&
         game_chain[0] != "") {
       win(game_chain[0]);
     }
     if (game_chain[2] == game_chain[4] &&
         game_chain[2] == game_chain[6] &&
         game_chain[2] != "") {
       win(game_chain[2]);
     }
      if (counter==9);

}
win(String winner){
  // socket.drain();
  socket.add(utf8.encode("win"));

   Navigator.pushReplacement(
    context,
    MaterialPageRoute(builder: (context) => win_page(winner)),);
  // if (myturn) {
  //
  //     print("myturn");
  //     myplay=["", "", "", "", "", "", "", "", ""];
  //
  //     socket.add(utf8.encode(myplay.toString()));
  //     myturn=false;
  //   }
  //   setState(() {  });
  // Navigator.pop(context);
}
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(

        title: Text(""),
      ),
      body: Column(
        children: [Container(),
          Expanded(flex: 4,
            child: GridView.builder(
                itemCount: 9,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3),
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    onTap: ()async{
                      if(myturn){
                        print("tapped");
                      tapped(index);
                     }
                    setState(() {});
                      },
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.blue)),
                      child: Center(
                        child: Text(
                          game_chain[index],
                          style: TextStyle(color: Colors.black, fontSize: 35),
                        ),
                      ),
                    ),
                  );
                }),
          ),
      //     StreamBuilder(
      // stream:socket.asBroadcastStream() ,
      // builder: (context, snapshot) {print(snapshot.data);
      //   return Text(snapshot.hasData ? '${snapshot.data}' : "");},),
          myturn?Text("play"):Text("wait"),
        ElevatedButton(onPressed: (){myplay=["", "", "", "", "", "", "", "", ""];
        socket.add(utf8.encode(myplay.toString()));
        myturn=false;
        setState(() {

        });}, child: Icon(Icons.refresh))],
      ),
    );
  }
}
