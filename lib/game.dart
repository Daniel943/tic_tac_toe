import 'dart:io';
import 'dart:math';
import 'package:confetti/confetti.dart';
import 'package:flutter/material.dart';

import 'main.dart';


class win_page extends StatefulWidget {
  var winner;
  win_page(this.winner);

  @override
  _win_pageState createState() => _win_pageState(winner);
}

class _win_pageState extends State<win_page> {
  ConfettiController controllerTopCenter;
  var winner;

  _win_pageState(this.winner);
  @override

  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      initController();
      controllerTopCenter.play();
    });

  }

  void initController() {
    controllerTopCenter =
        ConfettiController(duration: const Duration(seconds: 3));
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.cyan,
        title: Center(child: Text(winner.toString()+" is the winner")),
        automaticallyImplyLeading: false,
      ),

      body: SafeArea(
        child: Stack(
          children: <Widget>[
            buildConfettiWidget(controllerTopCenter, pi / 1),
            buildConfettiWidget(controllerTopCenter, pi / 4),
            Align(
                alignment: Alignment.center,
              child: Column(
                children: <Widget>[
                  Image.asset(
                    "assets/trophy.png",
                    width: MediaQuery.of(context).size.width*0.7,
                    height: MediaQuery.of(context).size.height*0.7,
                  ),
                ],
              ),
            ),
            // buildButton("Congratulations!"),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 100),
              child: RaisedButton(
                onPressed: ()async{
    socket = await Socket.connect('176.231.64.112', 8010);
    game_chain=["", "", "", "", "", "", "", "", ""];

    Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (BuildContext context) =>new Board(socket)));},

                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                color: Colors.red,
                textColor: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    "new game",
                    style: TextStyle(
                      fontSize: 30,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            )
            )],
        ),
      ),
    );
  }

  Align buildButton(String text) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 100),
        child: RaisedButton(
          onPressed: (){
            controllerTopCenter.play();

          },
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          color: Colors.red,
          textColor: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              text,
              style: TextStyle(
                fontSize: 30,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Align buildConfettiWidget(controller, double blastDirection) {
    return Align(
      alignment: Alignment.topCenter,
      child: ConfettiWidget(
        maximumSize: Size(30, 30),
        shouldLoop: false,
        confettiController: controller,
        blastDirection: blastDirection,
        blastDirectionality: BlastDirectionality.directional,
        maxBlastForce: 20, // set a lower max blast force
        minBlastForce: 8, // set a lower min blast force
        emissionFrequency: 1,
        numberOfParticles: 8, // a lot of particles at once
        gravity: 1,
      ),
    );
  }
}