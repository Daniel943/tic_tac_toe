import socket
import threading
from time import sleep

IP = "0.0.0.0"
PORT = 8010
clients = []
rooms = []


class Server(object):
    def __init__(self):
        self.server = socket.socket()
        self.server.bind((IP, PORT))
        self.server.listen()


class Consumer_Thread(threading.Thread):
    def __init__(self, server):
        self.clients = []
        threading.Thread.__init__(self)
        self.server = server

    def run(self):
        try:
            while self.clients.__len__() < 2:
                (client_socket, address) = self.server.accept()
                self.clients.append(client_socket)
                print(f"{self.clients}+k")
            print("start")
            self.clients[0].send("player x".encode())
            self.clients[1].send("player o".encode())
            rooms.append(self.clients)
            Consumer_Thread(self.server).start()

            while True:
                sleep(0.11)
                for client in self.clients:
                    client.send("!x".encode())
                x = self.clients[0].recv(1024)
                sleep(0.11)
                for client in self.clients:
                    client.send(x)
                print(x)
                sleep(0.11)
                for client in self.clients:
                    client.send("!o".encode())
                x = self.clients[1].recv(1024)
                sleep(0.11)
                for client in self.clients:
                    client.send(x)
                print(f"{x}+2")
                if x.decode() == "win":
                    for client in self.clients:
                        client.close()
        except:
            print("close")


def main():
    global clients
    global rooms
    my_server = Server()
    Consumer_Thread(my_server.server).start()


if __name__ == "__main__":
    main()
